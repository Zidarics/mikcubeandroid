package org.zamek.mikcube.util

import android.app.Activity
import android.app.AlertDialog
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.provider.Settings
import android.widget.Toast
import org.zamek.mikcube.R
import java.util.*
import kotlin.collections.ArrayList

/**
 * \brief
 * \author Zoltan Zidarics (Zamek)
 * \date 2020
 */
interface BluetoothUi {

    companion object {
        private val REQUEST_BLUETOOTH_PERMISSION = 42
        private val REQUEST_BLUETOOTH = 43
        private val REQUEST_LOCATION = 44
    }

    fun log(msg: String)

    fun getActivity(): Activity

    fun getBluetoothModel(): BluetoothModel

    fun getDeviceName(): String

    fun getServiceId(): UUID

    fun addApplicationPermissions(needPermissions:MutableList<String>)

    fun checkBluetoothPermissions() {
        log("------------------>>>BluetoothUi.checkBluetoothPermissions, enter")
        val needPermissions : MutableList<String> = getBluetoothModel().getRecommendedPersmissions().toMutableList()
        addApplicationPermissions(needPermissions)
        val deniedPermission = needPermissions.filter { getActivity().checkSelfPermission(it) != PackageManager.PERMISSION_GRANTED }

        if (deniedPermission.isEmpty()) {
            log("------------------>>>BluetoothUi.checkBluetoothPermissions, all needed permissions granted")
            turnOnBle()
            return
        }

        getActivity().apply {
            deniedPermission.forEach {
                log("------------------>>>BluetoothUi.checkBluetoothPermissions, permission needed:$it")
                if (shouldShowRequestPermissionRationale(it))
                    Toast.makeText(this, R.string.app_requires_bluetooth, Toast.LENGTH_SHORT)
                        .show()
            }

            requestPermissions(deniedPermission.toTypedArray(), REQUEST_BLUETOOTH_PERMISSION)
        }
    }

    fun permissionRequestResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        log("------------------>>>BluetoothUI.bluetoothRequestResult, requestCode:$requestCode")
        if (requestCode == REQUEST_BLUETOOTH_PERMISSION) {
            for (i in grantResults.indices) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    log("------------------>>>BluetoothUi.permissionRequestResult, ${permissions[i]} is not enabled")
                    bluetoothNotEnabled()
                    return
                }
            }
        }
        turnOnBle()
    }

    private fun bluetoothNotEnabled() {
        getBluetoothModel().noBle = true
        Toast.makeText(
            getActivity(),
            R.string.app_working_without_bluetooth,
            Toast.LENGTH_LONG
        ).show()
    }

    fun bluetoothRequestResult(requestCode: Int, requestResult: Int) {
        log("------------------>>>BluetoothUi.bluetoothRequestResult, requestCode:$requestCode, result:$requestResult")
        when (requestCode) {
            REQUEST_BLUETOOTH-> {
                if (requestResult == PackageManager.PERMISSION_DENIED) {
                    bluetoothNotEnabled()
                    return
                }
                turnOnLocation()
            }
            REQUEST_LOCATION -> {
                if (requestResult == PackageManager.PERMISSION_DENIED) {
                    bluetoothNotEnabled()
                    return
                }
                getBluetoothModel().scanningEnabled.set(true)
            }
        }
    }

    fun turnOnBle() {
        log("------------------>>>BluetoothUi.turnOnBle, enter")
        getBluetoothModel().bluetoothAdapter?.let {
            if (it.isEnabled)   // Bluetooth already enabled
                turnOnLocation()
            else
                getActivity().startActivityForResult(
                    Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE),
                    REQUEST_BLUETOOTH
                )
        }
    }

    fun turnOnLocation() {
        log("------------------>>>BluetoothUi.turnOnLocation, enter")
        val lm = getActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P) {
                lm.isLocationEnabled
            } else {
                TODO("VERSION.SDK_INT < P")
            }
        ) {
            log("------------------>>>BluetoothUi.turnOnLocation, location is enabled")
            getBluetoothModel().scanningEnabled.set(true)
            return
        }
        log("------------------>>>BluetoothUi.turnOnLocation, location is disabled")
        AlertDialog.Builder(getActivity())
            .setMessage(R.string.app_requires_location)
            .setPositiveButton(R.string.ok) { _, _ ->
                getActivity().startActivityForResult(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), REQUEST_LOCATION)
            }
            .setNegativeButton(R.string.cancel) { _, _ ->
                bluetoothNotEnabled()
            }
            .show()

    }

}


/*
//        val builder = LocationSettingsRequest.Builder()
//            .addLocationRequest(LocationRequest().apply {
//                setFastestInterval(10000)
//                    .setInterval(10000)
//                    .setPriority(LocationRequest.PRIORITY_LOW_POWER)
//            })

//        val result = LocationServices.getSettingsClient(getActivity())
//            .checkLocationSettings(builder.build())
//        result.addOnSuccessListener {
//            log("------------------>>>BluetoothUi.turnOnLocation, success")
//            getBluetoothModel().scanningEnabled.set(true)
//        }
//        result.addOnCanceledListener {
//            log("------------------>>>BluetoothUi.turnOnLocation cancelled")
//            bluetoothNotEnabled()
//        }

 */

