package org.zamek.mikcube.util

import android.graphics.Color
import android.util.Log

/**
 * \brief
 * \author Zoltan Zidarics (Zamek)
 * \date 2020
 */
const val OFF_COLOR : Int = 0x444444 // Color.DKGRAY
const val OFF_COLOR_OPACITY = 0.3f

typealias ColorArray = Array<FloatArray>

fun createColorArray() : ColorArray {
    return arrayOf(
        floatArrayOf(0f, 0f, 0f, 0f),
        floatArrayOf(0f, 0f, 0f, 0f),
        floatArrayOf(0f, 0f, 0f, 0f)
    )
}

fun createOffColorArray() : ColorArray {
    val c = createColorArray()
    convertColorArray(OFF_COLOR, OFF_COLOR_OPACITY, c)
    return c
}

fun convertColorArray(red: Int, green: Int, blue: Int, opacity: Float, array: ColorArray) {
    array[0][0]=red/255.0f
    array[0][1]=green/255.0f
    array[0][2]=blue/255.0f
    array[0][3]=opacity

    array[1][0]=array[0][0]*0.7f
    array[1][1]=array[0][1]*0.7f
    array[1][2]=array[0][2]*0.7f
    array[1][3]=array[0][3]

    array[2][0]=array[0][0]*0.9f
    array[2][1]=array[0][1]*0.9f
    array[2][2]=array[0][2]*0.9f
    array[2][3]=array[0][3]
}

fun convertColorArray(color: Int, opacity: Float, array: ColorArray) = convertColorArray(
    Color.red(color),
    Color.green(color),
    Color.blue(color),
    opacity,
    array
)

fun logColorArray(cubeColor: ColorArray) {
    for (i in 0 until 3)
        for (j in 0 until 4)
            Log.d("QQQ", "$i,$j:${cubeColor[i][j]}")
}

fun colorAtLightness(color: Int, lightness: Float): Int {
    val hsv = FloatArray(3)
    Color.colorToHSV(color, hsv)
    hsv[2] = lightness
    return Color.HSVToColor(hsv)
}