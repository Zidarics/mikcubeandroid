package org.zamek.mikcube.util

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.pm.PackageManager
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jakewharton.rx.ReplayingShare
import com.polidea.rxandroidble2.*
import com.polidea.rxandroidble2.exceptions.BleScanException.BLUETOOTH_NOT_AVAILABLE
import com.polidea.rxandroidble2.scan.ScanFilter
import com.polidea.rxandroidble2.scan.ScanResult
import com.polidea.rxandroidble2.scan.ScanSettings
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.zamek.mikcube.MikCubeApplication
import org.zamek.mikcube.model.NotConnectedException
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean


/**
 * \brief
 * \author Zoltan Zidarics (Zamek)
 * \date 2020
 */
abstract class BluetoothModel(
    private val deviceName: String,
    private val serviceId: UUID
)
    : ViewModel(), KoinComponent {

    private val TAG = BluetoothModel::class.java.name

    //If you want to use it in DEBUG mode temporally.
    // Must be beginnig with DEBUG_TEMPORARY because git can set it to false when commit
    private val DEBUG_TEMPORARY_TESTING = false

    private inline fun deb(msg: String) {
        if (DEBUG_TEMPORARY_TESTING) Log.d("QQQ", "--------> $msg")
    }

    private val SCAN_INTERVAL = 30000L

    private val rxBleClient : RxBleClient by inject()

    val bluetoothAdapter : BluetoothAdapter by lazy {
        val bluetoothManager = MikCubeApplication.applicationContext().getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothManager.adapter
    }

    private val connectionDisposable = CompositeDisposable()

    private val disconnectTriggerSubject = PublishSubject.create<Unit>()

    private var scanDisposable : Disposable?=null

    private var flowDisposable : Disposable?=null

    private var stateDisposable : Disposable?=null

    private lateinit var connectionObservable : Observable<RxBleConnection>

    val scanningEnabled = AtomicBoolean(false)

    private val _bleDevice = MutableLiveData<RxBleDevice>()
    val  bleDevice: LiveData<RxBleDevice>
        get() = _bleDevice

    private val _bluetoothState = MutableLiveData(RxBleConnection.RxBleConnectionState.DISCONNECTED)
    val bluetoothState : LiveData<RxBleConnection.RxBleConnectionState>
        get() = _bluetoothState

    private val scanStarted = AtomicBoolean(false)

    var noBle = false

    protected abstract fun characteristicReaded(bytes: ByteArray)

    protected abstract fun notificationSuccess(bytes: ByteArray);

    init {
        if (DEBUG_TEMPORARY_TESTING) {
            RxBleClient.updateLogOptions(
                LogOptions.Builder()
                    .setLogLevel(LogConstants.DEBUG)
                    .setMacAddressLogSetting(LogConstants.MAC_ADDRESS_FULL)
                    .setUuidsLogSetting(LogConstants.UUIDS_FULL)
                    .setShouldLogAttributeValues(true)
                    .build()
            )
        }

        startScanningService()
    }

    override fun onCleared() {
        super.onCleared()
        scanStarted.set(false)
        closeConnection()
        scanDisposable?.dispose()
        stateDisposable?.dispose()
    }

    fun isConnected() : Boolean {
        return _bleDevice.value?.let {
            return it.connectionState== RxBleConnection.RxBleConnectionState.CONNECTED
        } ?: false
    }

    private fun closeConnection() {
        deb("CubeModelRxBle.closeConnection")
        connectionDisposable.dispose()
    }

    private fun connected(scanResult: ScanResult) {
        deb("CubeModelRxBle.connected, scanResult:$scanResult")
        _bluetoothState.value = RxBleConnection.RxBleConnectionState.CONNECTED
        _bleDevice.value = scanResult.bleDevice
        connectionObservable = scanResult.bleDevice.establishConnection(true)
            .takeUntil(disconnectTriggerSubject)
            .compose(ReplayingShare.instance())
        connectGatt()
        _bleDevice.value?.let {
            stateDisposable = it.observeConnectionStateChanges()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onConnectionStateChange)
        }
    }

    private fun onConnectionStateChange(newState: RxBleConnection.RxBleConnectionState) {
        _bluetoothState.value=newState
    }

    private fun connectGatt() {
        if (noBle)
            return

        deb("CubeModelRxBle.connectGatt, enter")
        try {
            _bleDevice.value?.let { device->
                if (device.connectionState == RxBleConnection.RxBleConnectionState.CONNECTED)
                    triggerDisconnect()
                else {
                    connectionObservable
                        .flatMapSingle { it.discoverServices() }
                        .flatMapSingle { it.getCharacteristic(serviceId) }
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                            { gattConnected(it) },
                            { connectFailed(it) },
                            { }
                        ).let {
                            connectionDisposable.add(it)
                        }
                }
            }
        } catch (e: Exception) {
            Log.e(TAG, "connectGatt: ${e.message}")
        }
    }

    private fun triggerDisconnect() = disconnectTriggerSubject.onNext(Unit)

    private fun gattConnected(t: BluetoothGattCharacteristic) {
        deb("CubeModelRxBle.gattConnected, t:$t")
        readCharacteristic(serviceId)
        setNotification(serviceId)
    }

    @Throws(NotConnectedException::class)
    private fun readCharacteristic(uuid: UUID) {
        if (noBle)
            return

        _bleDevice.value?.let { it ->
            if (it.connectionState!= RxBleConnection.RxBleConnectionState.CONNECTED)
                throw NotConnectedException("ReadCharacteristic needs active connection")

            connectionObservable
                .firstOrError()
                .flatMap { it.readCharacteristic(uuid) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result -> characteristicReaded(result) },
                    { t -> connectFailed(t) }
                ).let {
                    connectionDisposable.add(it)
                }
        }?: throw NotConnectedException("readCharacteristic error, bleDevice is null")
    }

    @Throws(NotConnectedException::class)
    protected fun writeCharacteristic(uuid: UUID, value: ByteArray) {
        if (noBle)
            return

        _bleDevice.value?.let {
            if (it.connectionState != RxBleConnection.RxBleConnectionState.CONNECTED)
                throw NotConnectedException("WriteCharacteristic needs active connection")

            connectionObservable
                .firstOrError()
                .flatMap { it.writeCharacteristic(uuid, value) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { onWriteSuccess() },
                    { error -> onWriteFailed(error) })
                .let { connectionDisposable.add(it) }
        } ?: throw NotConnectedException("writeCharacteristic error, bleDevice is null")
    }

    @Throws(NotConnectedException::class)
    private fun setNotification(uuid: UUID) {
        if (noBle)
            return

        deb("CubeModelRxBle.setNotification on $uuid")

        _bleDevice.value?.let {
            if (it.connectionState != RxBleConnection.RxBleConnectionState.CONNECTED)
                throw NotConnectedException("setNotification needs active connection")

            connectionObservable
                .flatMap { it.setupNotification(uuid) }
                .flatMap { it }
                .doOnNext{ onNotificationHasBeenSetup() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { notificationSuccess(it) },
                    { error -> onNotificationFailed(error) }
                )
                .let { connectionDisposable.add(it) }
        }  ?: throw NotConnectedException("setNotification error, bleDevice is null")
    }

    protected fun onWriteSuccess(){
        deb("CubeModelRxBle.writeSuccess")
        Log.i(TAG, "writeSuccess")
    }

    protected fun onWriteFailed(t: Throwable) {
        deb("CubeModelRxBle.writeFailed, ${t.message}")
        Log.e(TAG, "writeFailed: ${t.message}")
    }

    protected fun onNotificationHasBeenSetup() {
        deb("CubeModelRxBle.notificationHasBeenSetup")
        Log.i(TAG, "notificationHasBeenSetup")
    }

    protected fun onNotificationFailed(t: Throwable) {
        deb("CubeModelRxBle.notificationFailed, ${t.message}")
        Log.e(TAG, "notificationFailed: ${t.message}")
    }

    fun ByteArray.toHex() = joinToString("") { String.format("%02X", (it.toInt() and 0xff)) }

    private fun scanFailed(t: Throwable?) {
        deb("CubeModelRxBle.scanFailed, t:$t")
        t?.let {
            _bluetoothState.value= RxBleConnection.RxBleConnectionState.DISCONNECTED
            _bleDevice.value = null
        }
        if (noBle)
            return
    }

    private fun connectFailed(t: Throwable?) {
        deb("CubeModelRxBle.connectFailed, t:$t")
        t?.let {
            _bluetoothState.value= RxBleConnection.RxBleConnectionState.DISCONNECTED
            _bleDevice.value = null
        }
    }

    private fun startScanningService() {
        viewModelScope.launch(Dispatchers.Main) {
            delay(1000)
            deb("BluetoothModel.startScanningService started, noBle:$noBle")
            while (!noBle) {
                scanDisposable?.dispose()
                deb("BluetoothModel.startScanningService, scanningEnabled:${scanningEnabled.get()}, connectionState:${_bleDevice.value?.connectionState}")
                if (_bleDevice.value?.connectionState!=RxBleConnection.RxBleConnectionState.CONNECTED &&
                    scanningEnabled.get() &&
                    rxBleClient.isScanRuntimePermissionGranted) {
                    deb("BluetoothModel.startScanningService scan, deviceName:$deviceName")
                    scanDisposable = rxBleClient.scanBleDevices(
                        ScanSettings.Builder()
                            .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
                            .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
                            .build(),
                        ScanFilter.Builder()
                            .setDeviceName(deviceName)
                            .build()
                    ).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe { deb("BluetoothModel.startBluetoothScan") }
                        .doOnError { t -> scanFailed(t) }
                        .subscribe(
                            { t: ScanResult? -> t?.let { connected(t) } },
                            { t: Throwable? -> scanFailed(t) }
                        )
                }
                else
                    scanNotStarted()

                delay(SCAN_INTERVAL)
            }
        }
    }

    private fun scanNotStarted() {
        if (_bleDevice.value?.connectionState==RxBleConnection.RxBleConnectionState.CONNECTED) {
            deb("BluetoothModel.scanNotStarted, because already connected")
            return
        }

        if (!scanningEnabled.get()) {
            deb("BluetoothModel.scanNotStarted, because scanning is not enabled")
            return
        }

        if (!rxBleClient.isScanRuntimePermissionGranted) {
            deb("BluetoothModel.scanNotStarted, because scnaRuntimePermission is not granted")
            return
        }

        deb("BluetoothModel.scanNotStarted, because WTF!")
    }

    fun getRecommendedPersmissions() = rxBleClient.recommendedScanRuntimePermissions

    fun isScanRuntimePermissions() = rxBleClient.isScanRuntimePermissionGranted

    fun isBleSupported(context: Context) : Boolean =
        BluetoothAdapter.getDefaultAdapter() != null &&
                context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)

}


