package org.zamek.mikcube.ui

import android.Manifest
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.os.StrictMode
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.polidea.rxandroidble2.RxBleConnection
import org.zamek.mikcube.R
import org.zamek.mikcube.model.CubeModel
import org.zamek.mikcube.databinding.ActivityMainBinding
import org.zamek.mikcube.util.BluetoothModel
import org.zamek.mikcube.util.BluetoothUi
import java.util.*

class MainActivity : AppCompatActivity(), PreferenceFragmentCompat.OnPreferenceStartFragmentCallback, BluetoothUi {

    private val TAG = "MainActivity"

    //If you want to use it in DEBUG mode temporally.
    // Must be beginnig with DEBUG_TEMPORARY because git can set it to false when commit
    val DEBUG_TEMPORARY_TESTING = false

    override fun log(msg: String) {
        if (DEBUG_TEMPORARY_TESTING) Log.d("QQQ", msg)
    }

    private lateinit var model : CubeModel
    private lateinit var binding : ActivityMainBinding
    private lateinit var navController:NavController

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder().build())

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        val toolBar = binding.toolbar
        setSupportActionBar(toolBar)
        supportActionBar?.apply {
            setDisplayShowTitleEnabled(false)
        }

        navController = this.findNavController(R.id.cube_nav_host_fragment)

        binding.bottomNavView.let { bottomNavView ->
            NavigationUI.setupWithNavController(bottomNavView, navController)
        }

        model = ViewModelProvider(this).get(CubeModel::class.java)

        model.bluetoothState.observe(this, {
            showConnected(it)
        })
        checkBluetoothPermissions()
    }

    override fun onResume() {
        super.onResume()
        if (model.isConnected())
            showConnected(RxBleConnection.RxBleConnectionState.CONNECTED)
    }

    private fun showConnected(state:RxBleConnection.RxBleConnectionState) {
         when (state) {
             RxBleConnection.RxBleConnectionState.CONNECTED -> binding.amToolbarContent.tbConnected.setImageResource(
                 R.drawable.ic_cube_connected
             )
             else -> binding.amToolbarContent.tbConnected.setImageResource(R.drawable.ic_cube_disconnected)
         }
    }

    override fun onPreferenceStartFragment(
                caller: PreferenceFragmentCompat?,
                pref: Preference?): Boolean {
        pref?.let {
            val navDest = navController.graph.find { target->
                it.fragment.endsWith(target.label?:"")
            }
            navDest?.let { target -> navController.navigate(target.id)  }
            return true
        }
        return false
    }

    override fun getActivity(): Activity = this

    override fun getBluetoothModel(): BluetoothModel = model

    override fun getDeviceName(): String = CubeModel.MIK_CUBE_DEVICE_NAME

    override fun getServiceId(): UUID = CubeModel.CHARACTERISTIC_STATE_UUID

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        log("------------------>>>MainActivity.onRequestPermissionsResult, $requestCode")
        permissionRequestResult(requestCode, permissions,grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        log("------------------>>>MainActivity.onActivityResult, request:$requestCode, result:$resultCode")
        bluetoothRequestResult(requestCode, resultCode)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun addApplicationPermissions(needPermissions: MutableList<String>) {
        needPermissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        needPermissions.add(Manifest.permission.INTERNET)
    }
}