package org.zamek.mikcube.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import org.zamek.mikcube.R
import org.zamek.mikcube.databinding.FragmentWebBinding

/**
 * \brief
 * \author Zoltan Zidarics (Zamek)
 * \date 2020
 */
class WebFragment : Fragment() {

    companion object {
        const val ATTR_URL = "url"
    }

    //If you want to use it in DEBUG mode temporally.
    // Must be beginnig with DEBUG_TEMPORARY because git can set it to false when commit
    private val DEBUG_TEMPORARY_TESTING = false

    @SuppressLint("LogNotTimber")
    private inline fun log(msg: String) {
        if (DEBUG_TEMPORARY_TESTING) Log.d("QQQ", msg)
    }

    private lateinit var binding: FragmentWebBinding
    private var url:String?=null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_web, container, false)

        arguments?.let {
            showWebView(url = it.getString(ATTR_URL).toString())
        }

        binding.fwClose.setOnClickListener {
            findNavController().popBackStack()
        }

        return binding.root
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun showWebView(url:String) {
        log("------------------>>>WebFragment.showWebView, url:$url")

        binding.fwWebview.apply {
            clearCache(true)
            clearFormData()
            clearHistory()
            settings.javaScriptEnabled = true
            loadUrl(url)
        }
    }
}