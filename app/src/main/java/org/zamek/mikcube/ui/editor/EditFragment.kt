package org.zamek.mikcube.ui.editor

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ShareCompat.IntentBuilder
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import com.developer.filepicker.model.DialogConfigs
import com.developer.filepicker.model.DialogProperties
import com.developer.filepicker.view.FilePickerDialog
import com.flask.colorpicker.ColorPickerView
import com.flask.colorpicker.builder.ColorPickerDialogBuilder
import org.zamek.mikcube.MikCubeApplication
import org.zamek.mikcube.R
import org.zamek.mikcube.databinding.FragmentEditBinding
import org.zamek.mikcube.model.Artifact
import org.zamek.mikcube.model.MCube
import org.zamek.mikcube.ui.OpenGLFragment
import org.zamek.mikcube.util.OFF_COLOR
import org.zamek.mikcube.util.colorAtLightness


class EditFragment : OpenGLFragment() {

    //If you want to use it in DEBUG mode temporally.
    // Must be beginnig with DEBUG_TEMPORARY because git can set it to false when commit
    private val DEBUG_TEMPORARY_TESTING = false

    private inline fun log(msg: String) {
        if (DEBUG_TEMPORARY_TESTING) Log.d("QQQ", msg)
    }

    private lateinit var binding : FragmentEditBinding
    private lateinit var model : DashboardModel
    private lateinit var editLedAdapter : EditLedAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = DashboardModel() {
            log("EditFragment.Refresh called")
            if (cubeView.getSceneRenderer()?.isLoad()!!) {
                for (i in 0 until MCube.MAX_PIXELS) {
                    cubeView.getSceneRenderer()?.setColor(i, it.getColorArray(i))
                }
                cubeView.requestRender()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        log("Enter EditFragment.onCreateView")

        model.filesDir = requireActivity().filesDir.absolutePath

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit, container, false)

        editLedAdapter = EditLedAdapter(requireContext(), model)

        binding.feDashboard.edEditPlain.edepContent.visibility = View.GONE
        binding.feDashboard.edEditPlain.edepGrid.adapter = editLedAdapter

        cubeView = binding.cubeView
        cubeView.init(supportOpenGLES, MikCubeApplication.applicationContext())

        initBindings()
        initObservers()

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        cubeView.onResume()
    }

    override fun onPause() {
        super.onPause()
        cubeView.onPause()
    }

    private fun updateState() {
        binding.feDashboard.apply {
            edDashboardPlain.apply {
                val pl = model.plain.value?:0
                edpPlainIncr.isEnabled = pl < model.artifact.plainCounts()-1
                model.plain.value?.let {
                    edpPlainDecr.isEnabled = it>0
                }
                edpPlainAdd.isEnabled = model.plainCounts()<Artifact.MAX_PLAIN-1
                edpPlainDelete.isEnabled = model.plainCounts()>1
            }
            edFramingTime.apply {
                model.frameTimeMs.value?.let {
                    edfFrameTimeDec.isEnabled = it>Artifact.MIN_DISPLAY_TIME_MS
                    edfFrameTimeInc.isEnabled = it<Artifact.MAX_DISPLAY_TIME_MS-1
                    return@apply
                }
                edfFrameTimeDec.isEnabled=false
                edfFrameTimeInc.isEnabled=false
                model.frameTimeMs.value?.let {
                    edpFrameTime.setText(it.toString())
                }
            }
        }

        binding.feDashboard.edStartStop.edssImageButton.apply {
            when {
                model.artifact.isPlaying() -> setImageDrawable(context.getDrawable(R.drawable.ic_baseline_stop_24))
                else -> setImageDrawable(context.getDrawable(R.drawable.ic_baseline_play_arrow_24))
            }
        }
    }

    private fun initDefaultsBinding() {
        //Defaults
        binding.feDashboard.edProgressBar.visibility=View.GONE
        updateState()
        binding.feDashboard.edDashboardColor.edcLed.setBackgroundColor(model.color.value ?: OFF_COLOR)
        binding.feDashboard.edFile.edbButtonShare.isEnabled = model.isFileExist()
        binding.feDashboardOpen.setOnClickListener {
            binding.drawerLayout.openDrawer(GravityCompat.START)
        }

        binding.drawerLayout.addDrawerListener(object: DrawerLayout.DrawerListener {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                //N.C
            }

            override fun onDrawerOpened(drawerView: View) {
                binding.feDashboardOpen.hide()
            }

            override fun onDrawerClosed(drawerView: View) {
                binding.feDashboardOpen.show()
            }

            override fun onDrawerStateChanged(newState: Int) {
                //N.C
            }
        })
    }

    private fun initColorDashboardBinding() {
        //Color dashboard
        binding.feDashboard.edDashboardColor.apply {
            edcOpenColorDlg.setOnClickListener {
                openColorPicker()
            }

            edcColor.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    //N.C
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    model.changeColor(s)
                }

                override fun afterTextChanged(s: Editable?) {
                    //N.C
                }
            })
        }
    }

    private fun initPlainDashboardBinding() {
        binding.feDashboard.edEditPlain.apply {
            edepClearAll.setOnClickListener {
                editLedAdapter.clearAll()
            }
            edepSetAll.setOnClickListener {
                model.color.value?.let {
                    editLedAdapter.setAll(it)
                }
            }

            edepGrid.setOnItemClickListener { _, _, position, _ ->
                val current = editLedAdapter.getItemColor(position)
                model.color.value?.let {
                    val transparent = OFF_COLOR
                    editLedAdapter.setItemColor(
                        position,
                        when (current) {
                            transparent -> it // it wasn't set earlier
                            else -> transparent
                        }
                    )
                }
            }
        }

        //Plain dashboard
        binding.feDashboard.edDashboardPlain.apply {
            edpPlainDecr.setOnClickListener{
                if (model.decPlain())
                    updateState()
            }

            edpPlainIncr.setOnClickListener{
                if (model.incPlain()) {
                    updateState()
                }
            }

            edpPlainAdd.setOnClickListener {
                if (model.addPlain())
                    updateState()
            }

            edpPlainDelete.setOnClickListener {
                if (model.deletePlain())
                    updateState()
            }

            edpPlainOpenEditor.setOnClickListener {
                binding.feDashboard.edEditPlain.edepContent.visibility = when(binding.feDashboard.edEditPlain.edepContent.visibility) {
                    View.VISIBLE -> View.GONE
                    else -> View.VISIBLE
                }
            }
        }

        binding.feDashboard.edFramingTime.apply {
            edfFrameTimeInc.setOnClickListener {
                if (model.incFrameTimeMs())
                    updateState()
            }

            edfFrameTimeDec.setOnClickListener {
                if (model.decFrameTimeMs())
                    updateState()
            }

            edpFrameTime.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    //N.C
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    //N.C
                }

                override fun afterTextChanged(s: Editable?) {
                    s?.let {
                        if (model.changeFrameTime(it))
                            updateState()
                    }
                }
            })
        }
    }

    private fun saveFile() {
        model.saveFile()
    }

    private fun deleteFile() {
        model.deleteFile()
    }

    private fun openFile() {
        val dlgProp = DialogProperties()
            dlgProp.selection_mode = DialogConfigs.SINGLE_MODE
            dlgProp.selection_type = DialogConfigs.FILE_SELECT
            dlgProp.root=model.artifact.getFile()
            dlgProp.offset=model.artifact.getFile()
            dlgProp.extensions = arrayOf(Artifact.FILE_EXTENSION)
            dlgProp.show_hidden_files = false
        val dlg = FilePickerDialog(requireContext(), dlgProp)
        dlg.setTitle(requireContext().getString(R.string.open_file_title))
        dlg.setDialogSelectionListener{
            model.saveFileAs(it[0])
        }
        dlg.show()
    }

    private fun shareFile() {
        val intentBuilder = IntentBuilder.from(requireActivity())
        intentBuilder.setType("text/json")
        intentBuilder.setSubject(this.getString(R.string.share_file_title))
        intentBuilder.setStream(model.getFileUri())
        intentBuilder.setChooserTitle(R.string.share_file_title)
        intentBuilder.createChooserIntent()
        val intent = intentBuilder.intent
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        this.startActivity(intent)
    }

    private fun initFileDashboardBinding() {
        //File dashboard
        binding.feDashboard.edFile.apply {
            edbButtonDelete.setOnClickListener {
                deleteFile()
            }

            edbButtonSave.setOnClickListener {
                saveFile()
            }

            edbButtonOpen.setOnClickListener {
                openFile()

            }

            edbButtonShare.setOnClickListener {
                shareFile()
            }

        }
    }

    private fun initBindings() {
        initDefaultsBinding()
        initColorDashboardBinding()
        initPlainDashboardBinding()
        initFileDashboardBinding()
        binding.feDashboard.edStartStop.edssImageButton.setOnClickListener{
            log("------------------->>>startstop button clicked")
            model.toggleStartStop()
            updateState()
        }
    }

    private fun colorChanged(color: Int) {
        binding.feDashboard.edDashboardColor.apply {
            edcColor.setText(model.getColorAsHex())
            edcLed.setBackgroundColor(color)
        }
    }

    private fun initObservers() {
        model.color.observe(viewLifecycleOwner, {
            log("------------------>>>EditFragment.init ColorObservers, color:${it.toString(16)}")
            colorChanged(it)
        })

        model.frameTimeMs.observe(viewLifecycleOwner, {
            binding.feDashboard.edFramingTime.edpFrameTime.setText(it.toString())
        })

        model.plain.observe(viewLifecycleOwner, {
            editLedAdapter.refresh()
            binding.feDashboard.edDashboardPlain.edpPlain.setText(it.toString())
        })

        model.fileName.observe(viewLifecycleOwner, {
            binding.feDashboard.edFile.edbFilename.setText(it)
        })

        model.progressBar.observe(viewLifecycleOwner, {
            log("Progressbar:$it")
            binding.feDashboard.edProgressBar.visibility = when(it) {
                0 -> View.GONE
                else -> View.VISIBLE
            }
        })
        model.animationStarted.observe(viewLifecycleOwner, {
            log("------------------>>>animationObserver, state:$it")
            when (it) {
                true -> {
                    cubeView.onResume()
                    binding.feDashboard.edStartStop.edssImageButton.setImageDrawable(MikCubeApplication.applicationContext().getDrawable(R.drawable.ic_baseline_stop_24))
                }
                else -> {
                    cubeView.onPause()
                    binding.feDashboard.edStartStop.edssImageButton.setImageDrawable(MikCubeApplication.applicationContext().getDrawable(R.drawable.ic_baseline_play_arrow_24))
                }
            }
        })
    }

    private fun openColorPicker() {
        val col = colorAtLightness(model.color.value?: OFF_COLOR, 1.0f)
        log("------------------>>>EditFragment.openColorPicker, color:${model.color.value!!.toString(16)}, col:${col.toString(16)}")
        ColorPickerDialogBuilder
            .with(context)
            .setTitle(R.string.choose_color)
            .initialColor(col)
            .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
            .density(12)
            .setPositiveButton(R.string.ok) { _, lastSelectedColor, _ -> model.changeColor(lastSelectedColor) }
            .setNegativeButton(R.string.cancel) { _, _ -> }
            .noSliders()
            .build()
            .show()
    }

}