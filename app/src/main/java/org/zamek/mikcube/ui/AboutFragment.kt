package org.zamek.mikcube.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import org.zamek.mikcube.R
import org.zamek.mikcube.databinding.FragmentAboutBinding
import org.zamek.mikcube.databinding.FragmentMainBinding

/**
 * \brief
 * \author Zoltan Zidarics (Zamek)
 * \date 2020
 */
class AboutFragment : Fragment() {
    //If you want to use it in DEBUG mode temporally.
    // Must be beginnig with DEBUG_TEMPORARY because git can set it to false when commit
    private val DEBUG_TEMPORARY_TESTING = false

    @SuppressLint("LogNotTimber")
    private inline fun log(msg: String) {
        if (DEBUG_TEMPORARY_TESTING) Log.d("QQQ", msg)
    }

    private lateinit var binding : FragmentAboutBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_about, container, false)
        binding.aUniversity.setOnClickListener {
            startWeb(getString(R.string.about_university_url))
        }
        binding.aFaculty.setOnClickListener {
            startWeb(getString(R.string.about_faculty_url))
        }
        binding.aEnergotest.setOnClickListener {
            startWeb(getString(R.string.about_energotest_url))
        }
        binding.aInstitute.setOnClickListener {
            startWeb(getString(R.string.about_institute_url))
        }
        return binding.root
    }

    private fun startWeb(url:String) {
        log("------------------>>>AboutFragment.startWeb, url:$url")
        val bundle = bundleOf(WebFragment.ATTR_URL to url)
        this.findNavController().navigate(R.id.webFragment, bundle)
    }
}