package org.zamek.mikcube.ui

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.zamek.mikcube.MikCubeApplication
import org.zamek.mikcube.R
import org.zamek.mikcube.opengl.SurfaceView

/**
 * \brief
 * \author Zoltan Zidarics (Zamek)
 * \date 2020
 */
open class OpenGLFragment : Fragment() {
    //If you want to use it in DEBUG mode temporally.
    // Must be beginnig with DEBUG_TEMPORARY because git can set it to false when commit
    private val DEBUG_TEMPORARY_TESTING = false

    @SuppressLint("LogNotTimber")
    private inline fun log(msg: String) {
        if (DEBUG_TEMPORARY_TESTING) Log.d("QQQ", msg)
    }

    protected lateinit var cubeView : SurfaceView

    protected val supportOpenGLES: Int
        get() {
            val am = this.activity?.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val info = am.deviceConfigurationInfo ?: return 1
            val version = java.lang.Double.parseDouble(info.glEsVersion)
            return when {
                version >= 3.0 -> 3 // or info.reqGlEsVersion >= 0x30000
                version >= 2.0 -> 2
                else -> 1
            }
        }

    protected lateinit var bottomNavigationView : BottomNavigationView

    protected fun createBottomView() {
        bottomNavigationView = requireActivity().findViewById(R.id.bottom_nav_view)
    }

    override fun onResume() {
        super.onResume()
        cubeView.onResume()
    }

    override fun onPause() {
        super.onPause()
        cubeView.onPause()
    }
}