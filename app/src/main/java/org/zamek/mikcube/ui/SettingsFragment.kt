package org.zamek.mikcube.ui

import android.os.Bundle
import android.util.Patterns
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.preference.EditTextPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager
import org.zamek.mikcube.R

class SettingsFragment : PreferenceFragmentCompat(), Preference.OnPreferenceChangeListener {

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
        bindPreferenceSummaryToValue(findPreference<EditTextPreference>(getString(R.string.pref_key_user_name)))
        bindPreferenceSummaryToValue(findPreference<EditTextPreference>(getString(R.string.pref_key_user_email)))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return super.onOptionsItemSelected(item)
    }

    private fun bindPreferenceSummaryToValue(preference:Preference?) {
        preference?.let {
            it.onPreferenceChangeListener = this
            onPreferenceChange(it,
                PreferenceManager.getDefaultSharedPreferences(it.context)
                    .getString(it.key, "")
            )
        }
    }

    override fun onPreferenceChange(preference: Preference?, value: Any?): Boolean {
        preference?.let {
            val stringValue = value.toString()
            if (it.key==requireContext().getString(R.string.pref_key_user_email)) {
                val pattern = Patterns.EMAIL_ADDRESS
                if (!pattern.matcher(stringValue).matches()) {
                    Toast.makeText(requireContext(), R.string.invalid_email, Toast.LENGTH_LONG).show()
                    return false
                }
            }
            it.summary = stringValue
            return true
        }
        return false
    }

}