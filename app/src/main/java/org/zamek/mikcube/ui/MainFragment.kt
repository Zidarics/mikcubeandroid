package org.zamek.mikcube.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import org.zamek.mikcube.MikCubeApplication
import org.zamek.mikcube.R
import org.zamek.mikcube.databinding.FragmentMainBinding
import org.zamek.mikcube.model.*

class MainFragment : OpenGLFragment() {

    //If you want to use it in DEBUG mode temporally.
    // Must be beginnig with DEBUG_TEMPORARY because git can set it to false when commit
    private val DEBUG_TEMPORARY_TESTING = false

    private inline fun log(msg: String) {
        if (DEBUG_TEMPORARY_TESTING) Log.d("QQQ", msg)
    }

    private lateinit var model : CubeModel
    private lateinit var binding : FragmentMainBinding
    private var embeddedTask : AbstractTask? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        model = activity?.run {
            ViewModelProvider(this).get(CubeModel::class.java)
        } ?: throw Exception("Invalid Activity")

        createBottomView()

        cubeView = binding.cubeView
        cubeView.init(supportOpenGLES, MikCubeApplication.applicationContext())

        val spinnerAdapter = ArrayAdapter(requireContext(), R.layout.spinner, model.getStateSpinnerItems())

        binding.figureChooser.scFigure.apply {
            adapter = spinnerAdapter
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {  //N.C
                }
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    val sel = selectedItem
                    model.changeFigure(sel as Figures)
                }
            }
        }

        binding.cubeRestorePosition.setOnClickListener{
            cubeView.restorePosition()
        }

        model.currentFigure.observe(viewLifecycleOwner, {
            log("observer fired")
            it?.let {
                log("state changed to ${it}, ordinal:${it.ordinal}")
                binding.figureChooser.scFigure.setSelection(it.ordinal)
                if (it!=Figures.PF_UNKNOWN) {
                    embeddedTask?.cancel()
                    embeddedTask = taskFactory(model, it, model.cube) {
                        if (cubeView.getSceneRenderer()?.isLoad()!!) {
                            for (i in 0 until MCube.MAX_PIXELS) {
                                cubeView.getSceneRenderer()?.setColor(i, it.getColorArray(i))
                            }
                            cubeView.requestRender()
                        }
                    }
                    embeddedTask?.start()
                }
            }
        })
        return binding.root
    }

    override fun onPause() {
        super.onPause()
        cubeView.onPause()
        embeddedTask?.pause()
    }

    override fun onResume() {
        super.onResume()
        cubeView.onResume()
        embeddedTask?.resume()
        model.currentFigure.value?.let {
            binding.figureChooser.scFigure.setSelection(it.ordinal)
        }
    }

}
