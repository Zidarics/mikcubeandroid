package org.zamek.mikcube.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import org.zamek.mikcube.R

/**
 * An example full-screen fragment that shows and hides the system UI (i.e.
 * status bar and bottom_navigation/system bar) with user interaction.
 */
class Splash : AppCompatActivity() {
    private val SPLASH_TIME_OUT = 30L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed({
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }, SPLASH_TIME_OUT)
    }
}