package org.zamek.mikcube.ui.editor

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import org.zamek.mikcube.R
import org.zamek.mikcube.model.Artifact
import org.zamek.mikcube.model.MCube
import org.zamek.mikcube.util.OFF_COLOR
import java.lang.Exception

/**
 * \brief
 * \author Zoltan Zidarics (Zamek)
 * \date 2020
 */
class EditLedAdapter(val context:Context, var model : DashboardModel) : BaseAdapter() {
    //If you want to use it in DEBUG mode temporally.
    // Must be beginnig with DEBUG_TEMPORARY because git can set it to false when commit
    private val DEBUG_TEMPORARY_TESTING = false

    private inline fun log(msg: String) {
        if (DEBUG_TEMPORARY_TESTING) Log.d("QQQ", msg)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view : View? = convertView
        if (convertView==null) {
            val inflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
             view = inflater.inflate(R.layout.edit_plain_item, parent, false)
        }
        view?.let {
            val led = view.findViewById<ImageView>(R.id.epi_led)
            model.plain.value?.let {
                led.setBackgroundColor(model.artifact.getColor(it, position))
            }
            return view
        }?: throw  Exception("No view found")
    }

    override fun getItem(position: Int): Any = model.plain.value?.let { model.artifact.getColor(it, position) }
                                                ?: model.artifact.getColor(0,position)

    override fun getItemId(position: Int): Long = position.toLong()


    override fun getCount(): Int = MCube.PIXELS_PER_PLAIN

    fun setItemColor(position:Int, color: Int) {
        if (position in 0 until MCube.PIXELS_PER_PLAIN) {
            model.plain.value?.let {
                model.artifact.setColor(it, position,color, 1.0f)
            }
            notifyDataSetChanged()
        }
    }

    fun getItemColor(position:Int) : Int =  model.plain.value?.let { model.artifact.getColor(it, position) }
                                                    ?: model.artifact.getColor(0,position)

    fun clearAll() {
        setAll(OFF_COLOR)
    }

    fun setAll(color:Int) {
        for (i in 0 until MCube.PIXELS_PER_PLAIN)
            model.plain.value?.let {
                model.artifact.setColor(it, i, color, 1.0f)
            }

        notifyDataSetChanged()
    }

    fun refresh() {
        notifyDataSetChanged()
    }
}