package org.zamek.mikcube.ui.editor

import android.graphics.Color

import android.net.Uri
import android.util.Log
import androidx.core.content.FileProvider
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.zamek.mikcube.BuildConfig
import org.zamek.mikcube.MikCubeApplication
import org.zamek.mikcube.model.*
import org.zamek.mikcube.util.OFF_COLOR
import java.io.File
import java.io.FileNotFoundException
import java.util.regex.Pattern

/**
 * \brief
 * \author Zoltan Zidarics (Zamek)
 * \date 2020
 */
class DashboardModel(var onRefreshListener : (cube:MCube) -> Unit) : ViewModel() {

    companion object {
        val COLOR_MASK = Pattern.compile("""^#([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})$""")
    }

    //If you want to use it in DEBUG mode temporally.
    //Must be beginnig with DEBUG_TEMPORARY because git can set it to false when commit
    private val DEBUG_TEMPORARY_TESTING = false

    private inline fun log(msg: String) {
        if (DEBUG_TEMPORARY_TESTING) Log.d("QQQ", msg)
    }

    private val SHARED_PROVIDER_AUTHORITY = BuildConfig.APPLICATION_ID + "cubefileprovider"

    val DEFAULT_FILE_NAME = "default"

    var filesDir : String?=null

    var artifact = Artifact(onRefreshListener)
        private set
        get() = field

    private val _plain = MutableLiveData(0)
    val plain : LiveData<Int>
        get() = _plain

    private val _color = MutableLiveData(OFF_COLOR)
    val color : LiveData<Int>
        get() = _color

    private val _frameTimeMs = MutableLiveData(Artifact.DEFAULT_DISPLAY_TIME_MS)
    val frameTimeMs : LiveData<Long>
        get() = _frameTimeMs

    private val _fileName = MutableLiveData(DEFAULT_FILE_NAME)
    val fileName : LiveData<String>
        get() = _fileName

    private val _progressBar = MutableLiveData(0)
    val progressBar : LiveData<Int>
        get() = _progressBar

    private var _plainType = MutableLiveData(MCube.PlainType.HORIZONTAL)
    val plainType : LiveData<MCube.PlainType>
        get()=_plainType

    private var _animationStarted = MutableLiveData(false)
    val animationStarted : LiveData<Boolean>
        get()= _animationStarted

    fun plainCounts() = artifact.plainCounts()

    fun isFileExist() : Boolean {
        _fileName.value?.let {
            val file = artifact.getFile()
            return  file.exists()
        }
        return false
    }

    private fun incProgressBar() {
        _progressBar.value?.let {
            _progressBar.value = it+1
        }
    }

    private fun  decProgressBar() {
        _progressBar.value?.let {
            if (it>0)
                _progressBar.value=it-1
        }
    }

    fun incPlain() : Boolean {
        _plain.value?.let {
            if (it<artifact.plainCounts()-1) {
                _plain.value = it + 1
                return true
            }
            return false
        }
        return false
    }

    fun decPlain() : Boolean {
        _plain.value?.let {
            if (it>0) {
                _plain.value = it - 1
                return true
            }
            return false
        }
        return false
    }

    fun addPlain() : Boolean {
        if (artifact.addPlain(frameTimeMs.value?:Artifact.MIN_DISPLAY_TIME_MS)) {
            _plain.value?.let {
                _plain.value = it+1
                return true
            }
            _plain.value=artifact.plainCounts()-1
            return true
        }
        return false
    }

    fun deletePlain() : Boolean {
        _plain.value?.let {
            if (artifact.deletePlain(it)) {
                if (it>0)
                    _plain.value = it-1
            }
            return true
        }
        return false
    }

    fun incFrameTimeMs() : Boolean {
        _frameTimeMs.value?.let { ms->
            if (ms<Artifact.MAX_DISPLAY_TIME_MS) {
                _frameTimeMs.value = ms + 1
                _plain.value?.let { pl->
                    return artifact.frameTimeMs(pl, ms)
                }
                return false
            }
            return false
        }
        return false
    }

    fun decFrameTimeMs() : Boolean {
        _frameTimeMs.value?.let { ms->
            if (ms>Artifact.MIN_DISPLAY_TIME_MS) {
                _frameTimeMs.value = ms - 1
                _plain.value?.let { pl->
                    return artifact.frameTimeMs(pl, ms)
                }
                return false
            }
            return false
        }
        return false
    }

    fun changeFrameTime(s:CharSequence) : Boolean {
        _plain.value?.let {
            val nv = s.toString().toLong()
            if (artifact.frameTimeMs(it,nv)) {
                _frameTimeMs.value=nv
                return true
            }
            return false
        }
        return false
    }

    fun changeColor(color:Int) {
        _color.value=color
    }

    fun getColorAsHex() : String {
        return color.value?.let {
            return String.format("#%02x%02x%02x", Color.red(it), Color.green(it), Color.blue(it) )
        } ?: OFF_COLOR.toString(16)
    }

    fun changeColor(color:CharSequence?) {
        color?.let {
            val matcher = COLOR_MASK.matcher(color)
            if (matcher.matches()) {
                val r = Integer.parseInt(matcher.group(1)?:"0", 16)
                val g = Integer.parseInt(matcher.group(2)?:"0", 16)
                val b = Integer.parseInt(matcher.group(3)?:"0", 16)
               val c = Color.rgb(r,g,b)
               _color.value?.let {
                   if (it==c)
                       return
               }
               _color.value = c
            }
        }
    }

    fun getMaxFrames() : Int = 0 //TODO artifact.getNumberOfPlains()

    fun saveFile() {
        try {
            incProgressBar()
            artifact.saveFile()
        }
        finally {
            decProgressBar()
        }
    }

    fun saveFileAs(filename:String) {
        try {
            incProgressBar()
            _fileName.value?.let {
                artifact.saveFileAs(it)
            }
        }
        finally {
            decProgressBar()
        }
    }

    fun loadFile(fileName: String) {
        _fileName.value?.let{
            try {
                incProgressBar()
                artifact = Artifact.loadArtifact("${MikCubeApplication.applicationContext().filesDir}$fileName", onRefreshListener)
                _fileName.value = fileName
            }
            finally {
                decProgressBar()
            }
        }
    }

    fun deleteFile() {
        _fileName.value?.let {
            if(DEFAULT_FILE_NAME==it)
                return
            try {
                incProgressBar()
                val file = File(MikCubeApplication.applicationContext().filesDir, "$it${Artifact.FILE_EXTENSION}")
                file.delete()
            }
            finally {
                decProgressBar()
            }
        }
    }

    fun getFileUri() : Uri {
        _fileName.value?.let {
            return FileProvider.getUriForFile(MikCubeApplication.applicationContext(), "org.zamek.mikcube.fileprovider", artifact.getFile())
        }?:throw FileNotFoundException("No filename")
    }

    fun toggleStartStop() {
        log("------------------>>>DashboardModel.toggleStartStop")
        artifact.toggleStartStop()
        _animationStarted.value=artifact.isPlaying();
    }

}