package org.zamek.mikcube

import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.PropertyAccessor
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule
import com.polidea.rxandroidble2.RxBleClient
import org.koin.dsl.module
import org.zamek.mikcube.model.Artifact
import java.text.SimpleDateFormat

/**
 * \brief
 * \author Zoltan Zidarics (Zamek)
 * \date 2020
 */
private val rxBleClient = RxBleClient.create(MikCubeApplication.applicationContext())

val earlyNeeded = module(createdAtStart = true) {
    single { rxBleClient }
    single { MikCubeApplication.objectMapper }
}

