package org.zamek.mikcube

import android.app.Application
import android.content.Context
import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.PropertyAccessor
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule
import com.polidea.rxandroidble2.RxBleClient
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.zamek.mikcube.model.Artifact
import java.text.SimpleDateFormat

/**
 * \brief
 * \author Zoltan Zidarics (Zamek)
 * \date 2020
 */


class MikCubeApplication : Application() {

    companion object {
        private var instance: MikCubeApplication? = null
        fun applicationContext() : Context = instance!!.applicationContext

        val objectMapper: ObjectMapper = ObjectMapper()
            .setDateFormat(SimpleDateFormat(Artifact.JSON_DATE_FORMAT))
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true)
            .disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE)
            .enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT)
            //.registerModule(JavaTimeModule())  //TODO crash !!
            .registerModule(ParameterNamesModule())
            .registerModule(Jdk8Module())
            .registerKotlinModule()
            .setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)
            .setVisibility(PropertyAccessor.CREATOR, JsonAutoDetect.Visibility.NONE)
            .setVisibility(PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE)
            .setVisibility(PropertyAccessor.SETTER, JsonAutoDetect.Visibility.NONE)
            .setVisibility(PropertyAccessor.IS_GETTER, JsonAutoDetect.Visibility.NONE)

    }


    init {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()
        instance=this
        startKoin {
            androidContext(this@MikCubeApplication)
            modules(listOf(earlyNeeded))
        }
    }
}