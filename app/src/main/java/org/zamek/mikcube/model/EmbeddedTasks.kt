package org.zamek.mikcube.model

import android.graphics.Color
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.zamek.mikcube.MikCubeApplication
import org.zamek.mikcube.R
import java.lang.Exception
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.jvm.Throws
import kotlin.math.pow
import kotlin.math.roundToInt
import kotlin.math.sin
import kotlin.math.sqrt

/**
 * \brief
 * \author Zoltan Zidarics (Zamek)
 * \date 2020
 */

class UnknownFigure(msg:String) : Exception(msg)

abstract class AbstractTask(val model:ViewModel,
                                      val cube : MCube,
                                      val onRefreshListener : (cube:MCube) -> Unit)
{

    private val keepRunning = AtomicBoolean(true)

    val isRunning = keepRunning.get()

    protected val paused = AtomicBoolean(false)

    protected var color:Int = Color.RED

    fun start() {
        model.viewModelScope.launch(Dispatchers.Main) {
            while (keepRunning.get()) {
                if (!paused.get()) {
                    cube.clear()
                    doIt()
                    onRefreshListener(cube)
                }
                delay(getFrameDelay())
            }

        }.start()
    }

    fun cancel() {
        keepRunning.set(false)
    }

    fun pause() {
        paused.set(true)
    }

    fun resume() {
        paused.set(false)
    }

    protected fun nextColor() {
        val c = Color.valueOf(color)
        color = Color.rgb(
            (c.red()+0x10)%0xff,
            (c.green()+0x10)%0xff,
            (c.blue()+0x10)%0xff)
    }

    protected abstract suspend fun doIt()

    protected abstract fun getFrameDelay() : Long

    abstract fun getName():String;
}

class OnOffEachLeds(model:ViewModel,
                              cube:MCube,
                              onRefreshListener : (cube:MCube) -> Unit)
    : AbstractTask(model, cube, onRefreshListener) {

    private var index:Int=0

    override suspend fun doIt() {
        cube.setColor(index, color, 1.0f)
        index = (index+1) % MCube.MAX_PIXELS
        if (index==0)
            nextColor()
    }

    override fun getName(): String = MikCubeApplication.applicationContext().getString(R.string.pf_on_off_each_led)

    override fun getFrameDelay() = cube.frameTime
}

open class ShiftHorizontalPlains(model:ViewModel,
                                           cube:MCube,
                                           onRefreshListener : (cube:MCube) -> Unit)
    : AbstractTask(model, cube, onRefreshListener) {

    private var plain = 0

    protected var plainType = MCube.PlainType.HORIZONTAL

    override suspend fun doIt() {
        if (plain==0)
            nextColor()

        for (i in 0 until MCube.PIXELS_PER_PLAIN)
            cube.setColor(plain, plainType, i, color, 1.0f)

        plain = (plain+1) % MCube.MAX_PLAINS
        if (plain==0)
            nextColor()
    }

    override fun getName(): String = MikCubeApplication.applicationContext().getString(R.string.pf_shift_horizontal_plains)

    override fun getFrameDelay() = cube.frameTime * 4L

}

class ShiftVerticalPlains(model:ViewModel,
                                    cube:MCube,
                                    onRefreshListener : (cube:MCube) -> Unit)
    : ShiftHorizontalPlains(model, cube, onRefreshListener)  {

    init {
        plainType = MCube.PlainType.VERTICAL
    }

    override fun getName(): String = MikCubeApplication.applicationContext().getString(R.string.pf_shift_vertical_plains)
}

class ShowText(model:ViewModel,
                         cube:MCube,
                         onRefreshListener : (cube:MCube) -> Unit)
    : AbstractTask(model, cube, onRefreshListener) {

    val text = "PTE-MIK"
    var plainIdx = 0
    var textIdx = 0

    override suspend fun doIt() {
        renderChar(plainIdx, text.get(textIdx), color)
        plainIdx = (plainIdx+1) % MCube.MAX_PLAINS
        if (plainIdx==0)
            textIdx = (textIdx+1) % text.length
    }

    private fun renderChar(plain:Int, ch:Char, color: Int) {
        val bytes = when(FONT_8X8_BASIC.containsKey(ch)) {
            true -> FONT_8X8_BASIC[ch]
            else -> UNKNOWN_CHARACTER
        }
        for (r in 0 until MCube.MAX_ROWS) {
            val b = bytes!![MCube.MAX_ROWS-1-r]
            for (c in 0 until MCube.MAX_COLUMNS) {
                if (b and (1 shl c) != 0) {
                    cube.setColor(plain, MCube.PlainType.VERTICAL, r, MCube.MAX_COLUMNS-c-1, color, 1.0f)
                }
            }
        }
        nextColor()
    }

    override fun getName(): String = MikCubeApplication.applicationContext().getString(R.string.pf_show_text)

    override fun getFrameDelay() = cube.frameTime * 6L
}

class DrawCircles(model:ViewModel,
                            cube:MCube,
                            onRefreshListener : (cube:MCube) -> Unit)
    : AbstractTask(model, cube, onRefreshListener)  {

    var plain=0
    var phase = 3

    override suspend fun doIt() {
       cube.drawCircle(plain, MCube.PlainType.HORIZONTAL,  3, 4, phase, color, 1.0f)
       nextColor()
       phase--
       if (phase==0) {
           phase=3
           plain=(plain+1)%MCube.MAX_PLAINS
       }
    }

    override fun getName(): String = MikCubeApplication.applicationContext().getString(R.string.pf_circle)

    override fun getFrameDelay()=cube.frameTime*2L
}

class DrawLines(model:ViewModel,
                          cube:MCube,
                          onRefreshListener : (cube:MCube) -> Unit)
    : AbstractTask(model, cube, onRefreshListener)  {

    var phase = 0
    var section = 0
    var plainType:MCube.PlainType=MCube.PlainType.VERTICAL

    override suspend fun doIt() {
        if (section==0)
            for (plain in 0 until MCube.MAX_PLAINS)
                cube.drawLine(plain, plainType, 0,phase,7,7-phase, color,1.0f)
        else
            for (plain in 0 until MCube.MAX_PLAINS)
                cube.drawLine(plain, plainType, 7-phase,0,phase, 7, color,1.0f)

        phase=(phase+1)%8
        section = (section+1) % 2
        if (phase==0) {
            plainType = when(plainType) {
                MCube.PlainType.VERTICAL-> MCube.PlainType.HORIZONTAL
                else -> MCube.PlainType.VERTICAL
            }
            nextColor()
        }
    }


    override fun getName(): String = MikCubeApplication.applicationContext().getString(R.string.pf_lines)

    override fun getFrameDelay() = cube.frameTime * 8L
}

class DrawSineCurve(model:ViewModel,
                              cube:MCube,
                              onRefreshListener : (cube:MCube) -> Unit)
    : AbstractTask(model, cube, onRefreshListener)  {

    private val phaseStep=Math.PI/8

    var phase = 0.0

    override suspend fun doIt() {
        for (x in 0 until MCube.MAX_ROWS) {
            for (y in 0 until MCube.MAX_COLUMNS) {
                val z = sin(
                    phase + sqrt(
                        map(x.toDouble(), 0.0, (MCube.MAX_ROWS - 1).toDouble() - 1.0, -Math.PI, Math.PI).pow(2.0) +
                        map(y.toDouble(), 0.0, (MCube.MAX_COLUMNS - 1).toDouble() - 1.0, -Math.PI, Math.PI).pow(2.0)
                    )
                )
                val zz = map(z, -1.0, 1.0, 0.0, (MCube.MAX_ROWS - 1).toDouble()).roundToInt()
                cube.setColor(zz, MCube.PlainType.HORIZONTAL, x, y, color, 1.0f)
            }
        }
        if (phase<2*Math.PI)
            phase+=phaseStep
        else {
            phase = 0.0
            nextColor()
        }
    }

    private inline fun map(inp:Double, inpMin:Double, inpMax:Double, outMin:Double, outMax:Double) =
        (inp-inpMin)/(inpMax-inpMin) * (outMax-outMin) + outMin

    override fun getName(): String = MikCubeApplication.applicationContext().getString(R.string.pf_sine)

    override fun getFrameDelay() = cube.frameTime * 4L
}


@Throws(UnknownFigure::class)
 fun taskFactory(model:ViewModel,
                type: Figures,
                cube : MCube,
                onRefreshListener : (cube:MCube) -> Unit)
        : AbstractTask = when (type) {
    Figures.PF_ON_OFF_EACH_LED -> OnOffEachLeds(model, cube, onRefreshListener)
    Figures.PF_SHIFT_HORIZONTAL_PLAINS -> ShiftHorizontalPlains(model, cube, onRefreshListener)
    Figures.PF_SHIFT_VERTICAL_PLAINS -> ShiftVerticalPlains(model, cube, onRefreshListener)
    Figures.PF_SHOW_TEXT -> ShowText(model, cube, onRefreshListener)
    Figures.PF_CIRCLE -> DrawCircles(model, cube, onRefreshListener)
    Figures.PF_LINES -> DrawLines(model, cube, onRefreshListener)
    Figures.PF_SINE -> DrawSineCurve(model, cube, onRefreshListener)
    else -> throw UnknownFigure("Unknown figure: type")
}
