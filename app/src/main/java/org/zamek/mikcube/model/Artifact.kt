package org.zamek.mikcube.model

import android.util.Log
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.databind.ObjectMapper
import kotlinx.coroutines.*
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.zamek.mikcube.MikCubeApplication
import org.zamek.mikcube.util.OFF_COLOR
import org.zamek.mikcube.util.OFF_COLOR_OPACITY
import java.io.File
import java.time.LocalDateTime
import java.util.ArrayList
import kotlin.random.Random

/**
 * \brief
 * \author Zoltan Zidarics (Zamek)
 * \date 2020
 */
class Artifact(var onRefreshListener : (cube:MCube) -> Unit) : KoinComponent {

    private var fileName:String=DEFAULT_FILE_NAME

    companion object {
        const val DEFAULT_FILE_NAME = "default"
        const val FILE_EXTENSION = ".mikcube"
        const val MAX_PLAIN = 100
        const val MIN_DISPLAY_TIME_MS = 50L
        const val MAX_DISPLAY_TIME_MS = 15000L
        const val DEFAULT_DISPLAY_TIME_MS = 150L

        const val ANONYMOUS = "Anonymous"
        const val JSON_DATE_FORMAT = "yyyy-MM-dd"

        fun loadArtifact(fileName: String, onRefreshListener : (cube:MCube) -> Unit) : Artifact {
            val file = File(MikCubeApplication.applicationContext().filesDir, fileName)
            if (file.exists()) {
                val result = MikCubeApplication.objectMapper.readValue(file, Artifact::class.java)
                result.fileName = fileName
                return result
            }
            return Artifact(onRefreshListener)
        }
    }

    //If you want to use it in DEBUG mode temporally.
    // Must be beginnig with DEBUG_TEMPORARY because git can set it to false when commit
    @JsonIgnore
    private val DEBUG_TEMPORARY_TESTING = false

    private inline fun log(msg: String) {
        if (DEBUG_TEMPORARY_TESTING) Log.d("QQQ", msg)
    }

    private  val TAG = "Artifact"
    
    private val DEBUG=true

    private val objectMapper: ObjectMapper by inject()

    private lateinit var owner: String

    private lateinit var ownerEmail: String

    private var created: LocalDateTime = LocalDateTime.now()

    private val plains = ArrayList<ArtifactPlain>(1)

    private var job:Job?=null

    var cube:MCube=MCube()

    init {
        if (!DEBUG)
            plains.add(ArtifactPlain())
        else {
            val random = Random(System.currentTimeMillis())
            for (i in 0 until 12) {
                val color=random.nextInt()
                Log.d(TAG, "color: $color")
                plains.add(ArtifactPlain(debug = DEBUG, displayTime = (100+ i * 100).toLong(), color))
            }
        }
    }

    fun plainCounts() = plains.size

    fun addPlain(displayTime:Long) : Boolean {
        log("addPlain() called with: displayTime = $displayTime, plains.size:${plains.size}")
        if (plains.size<MAX_PLAIN) {
            plains.add(ArtifactPlain(debug = DEBUG, displayTime=displayTime))
            return true
        }
        return false
    }

    fun deletePlain(index:Int) :Boolean {
        log("deletePlain() called with: index = $index")
        if (index<plains.size && plains.size>1) {
            plains.remove(plains.get(index))
            return true
        }
        return false
    }

    fun setColor(plain:Int, position:Int, color:Int, opacity:Float) {
        plains[plain].setColor(position, color, opacity)
    }

    fun setColor(plain: Int, row:Int, column:Int, color: Int, opacity: Float) {
        setColor(plain, row*MCube.MAX_ROWS+column, color, opacity)
    }

    fun getColor(plain: Int, position: Int) = plains[plain].getColor(position)

    fun getFile(): File =
        File(MikCubeApplication.applicationContext().filesDir, "$fileName$FILE_EXTENSION")

    fun saveFile() {
        objectMapper.writeValue(getFile(), this)
    }

    fun saveFileAs(name: String) {
        fileName = name
        saveFile()
    }

    private inline fun getPhysicalIndex(virtual: Int) : Int {
        val indexInPlain = virtual% MCube.PIXELS_PER_PLAIN
        return (indexInPlain% MCube.MAX_ROWS)* MCube.PIXELS_PER_PLAIN +
                (virtual/ MCube.PIXELS_PER_PLAIN)* MCube.MAX_COLUMNS +
                (indexInPlain/ MCube.MAX_COLUMNS)
    }

    fun isPlaying() : Boolean = job?.isActive?:false

    fun toggleStartStop() {
        log("------------------>>>toggleStartStop: enter")
        job?.let {
            if (it.isActive) {
                log("------------------>>>toggleStartStop: active, cancelling...")
                it.cancel()
            }
            job=null
            return
        }

        job=GlobalScope.launch (Dispatchers.Main) {
            startPlay()
        }
    }

    suspend private fun startPlay() {
        log("------------------>>>startPlay: enter")
        cube.let {cb->
            job?.let { jb ->
                while (jb.isActive) {
                    log("plains.size:${plains.size}")
                    var i=0;
                    while (i < plains.size) {
                        playFromHere(i)
                        i+=MCube.MAX_PLAINS;
                    }
                    cb.clear()
                    if (plains.size%MCube.MAX_PLAINS!=0) {
                        log("plains.size%MCube.MAX_PLAINS: ${plains.size%MCube.MAX_PLAINS} i:$i")
                        playFromHere(i - MCube.MAX_PLAINS);
                    }
                }
            }
        }
    }

    private suspend fun playFromHere(index:Int) {
        log("------------------>>>playFromHere, index:$index")
        cube.let {
            for (i in 0 until MCube.MAX_PLAINS) {
                log("playFromHere for index:$index, i:$i")
                var d = MIN_DISPLAY_TIME_MS
                if ((index+i)<plains.size) {
                    copyPlainData(index+i,i)
                    d=plains[index+i].getDisplayTime()
                    //log("----------->>> delay in if: $d")
                }
                else
                    copyEmptyData(i)
                onRefreshListener(cube)
                //log("----------->>> delay at $d")
                delay(d)
                it.clear()
            }
        }
    }

    private fun copyPlainData(plainIdx:Int, cubeIdx:Int) {
        log("------------------>>>copyPlainData enter, plainIdx:$plainIdx, cubeIdx:$cubeIdx")
        cube.let { c->
            for (i in 0 until MCube.PIXELS_PER_PLAIN) {
                var col = plains[plainIdx].getColor(i)
               // log("--------------->>>i:$i -> ${col.toString(16)}")
                c.setColor(cubeIdx, MCube.PlainType.HORIZONTAL, i, col, 1.0f)
            }
        }
    }

    private fun copyEmptyData(index: Int) {
        log("------------------>>>copyEmptyData, index:$index")
        cube.let {
            for (i in 0 until  MCube.PIXELS_PER_PLAIN)
                it.setColor(index, MCube.PlainType.HORIZONTAL, i,  OFF_COLOR, OFF_COLOR_OPACITY)
        }
    }

    fun frameTimeMs(plain:Int, ms:Long) : Boolean {
        if (plain>=0 && plain<plains.size)
            return plains[plain].setDisplayTime(ms)

        return false
    }
}

