package org.zamek.mikcube.model

import android.annotation.SuppressLint
import android.util.Log
import org.zamek.mikcube.util.OFF_COLOR
import java.time.LocalDateTime
import kotlin.jvm.Throws
import kotlin.random.Random

/**
 * \brief
 * \author Zoltan Zidarics (Zamek)
 * \date 2020
 */
class ArtifactPlain(debug : Boolean=false, private var displayTime:Long=Artifact.MIN_DISPLAY_TIME_MS, color:Int=OFF_COLOR) {
    //If you want to use it in DEBUG mode temporally.
    // Must be beginnig with DEBUG_TEMPORARY because git can set it to false when commit
    private val DEBUG_TEMPORARY_TESTING = false

    private val TAG = "ArtifactPlain"

    @SuppressLint("LogNotTimber")
    private inline fun log(msg: String) {
        if (DEBUG_TEMPORARY_TESTING) Log.d("QQQ", msg)
    }

    private val pixels = Array(MCube.PIXELS_PER_PLAIN) { Pixel() }

    init {
        if (debug) {
            Log.d(TAG, "ArtifactPlain, color: $color")
            for(i in 0 until MCube.PIXELS_PER_PLAIN) {
                pixels.set(i,Pixel(color));
            }
        }
    }

    fun setDisplayTime(nv:Long) : Boolean {
        if (nv!=displayTime && nv>=Artifact.MIN_DISPLAY_TIME_MS && nv <= Artifact.MAX_DISPLAY_TIME_MS) {
            displayTime=nv
            return true
        }
        return false
    }

    fun getDisplayTime() = displayTime

    @Throws(ArrayIndexOutOfBoundsException::class)
    fun setColor(position:Int, color: Int, opacity:Float) {
        pixels[position].setColor(color, opacity)
    }

    fun getColor(position: Int) = pixels[position].color

    fun getColorArray(position: Int) = pixels[position].colorArray

}