package org.zamek.mikcube.model

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import org.koin.core.KoinComponent
import org.zamek.mikcube.util.BluetoothModel
import java.util.*

/**
 * \brief
 * \author Zoltan Zidarics (Zamek)
 * \date 2020
 */

@Suppress("SENSELESS_COMPARISON")
class CubeModel : BluetoothModel(MIK_CUBE_DEVICE_NAME, CHARACTERISTIC_STATE_UUID),  KoinComponent {

    companion object {
        val MIK_CUBE_DEVICE_NAME = "MikCube"
        //Read-Write only characteristic providing the state of the figure
        val CHARACTERISTIC_STATE_UUID = UUID.fromString("0000ff01-0000-1000-8000-00805f9b34fb")
        // val SERVICE_UUID = UUID.fromString("000000ff-0000-1000-8000-00805f9b34fb")
    }

    //If you want to use it in DEBUG mode temporally.
    // Must be beginnig with DEBUG_TEMPORARY because git can set it to false when commit
    private val DEBUG_TEMPORARY_TESTING = false

    @SuppressLint("LogNotTimber")
    private inline fun log(msg: String) {
        if (DEBUG_TEMPORARY_TESTING) Log.d("QQQ", msg)
    }

    private val _currentFigure = MutableLiveData(Figures.PF_ON_OFF_EACH_LED)
    val currentFigure : LiveData<Figures>
        get() = _currentFigure

    val cube = MCube()

    fun changeFigure(figure:Figures) {
        _currentFigure.value?.let {
            if (it.equals(figure))
                return
        }
        log("------------------>>>CubeModelRxBle.changeFigure, figure:$figure, ordinal: ${figure.ordinal.toByte()}")

        if (noBle || !isConnected())
            _currentFigure.value=figure
        else
            writeCharacteristic(CHARACTERISTIC_STATE_UUID, byteArrayOf(figure.ordinal.toByte()))
    }

    fun getStateSpinnerItems() : List<Figures> = Figures.values().toList()

    override fun characteristicReaded(bytes: ByteArray) {
        _currentFigure.value = Figures.getByIndex(bytes[0])
    }

    override fun notificationSuccess(bytes: ByteArray) {
        _currentFigure.value = Figures.getByIndex(bytes[0])
    }
}