package org.zamek.mikcube.model

import android.annotation.SuppressLint
import android.util.Log
import org.zamek.mikcube.util.*

/**
 * \brief
 * \author Zoltan Zidarics (Zamek)
 * \date 2020
 */
class Pixel(color:Int=OFF_COLOR) {
    //If you want to use it in DEBUG mode temporally.
    // Must be beginnig with DEBUG_TEMPORARY because git can set it to false when commit
    private val DEBUG_TEMPORARY_TESTING = false

    @SuppressLint("LogNotTimber")
    private inline fun log(msg: String) {
        if (DEBUG_TEMPORARY_TESTING) Log.d("QQQ", msg)
    }

    var color:Int=color

    set(value) {
        field=value
        convertColorArray(this.color, this.opacity, this.colorArray)
    }

    val colorArray : ColorArray = createColorArray()
        get() = field

    var opacity:Float= OFF_COLOR_OPACITY
        get() = field
        set(opacity) {
            field = opacity
            convertColorArray(this.color, this.opacity, this.colorArray)
        }

    init {
        log("Pixel.init, color:$color")
        convertColorArray(this.color, this.opacity, this.colorArray)
    }

    fun setColor(color:Int, opacity:Float) {
        this.color=color
        this.opacity = opacity
        convertColorArray(this.color, this.opacity, colorArray)
    }
}