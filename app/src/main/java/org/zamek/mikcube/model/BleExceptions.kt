package org.zamek.mikcube.model

import java.lang.Exception

/**
 * \brief
 * \author Zoltan Zidarics (Zamek)
 * \date 2020
 */
class NotConnectedException(msg:String) : Exception(msg)

