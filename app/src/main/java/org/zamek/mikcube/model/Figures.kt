package org.zamek.mikcube.model

import org.zamek.mikcube.MikCubeApplication
import org.zamek.mikcube.R

/**
 * \brief
 * \author Zoltan Zidarics (Zamek)
 * \date 2020
 */
enum class Figures(val figureName:Int) {
    PF_ON_OFF_EACH_LED(R.string.pf_on_off_each_led),
    PF_SHIFT_HORIZONTAL_PLAINS(R.string.pf_shift_horizontal_plains),
    PF_SHIFT_VERTICAL_PLAINS(R.string.pf_shift_vertical_plains),
    PF_SHOW_TEXT(R.string.pf_show_text),
    PF_CIRCLE(R.string.pf_circle),
    PF_LINES(R.string.pf_lines),
    PF_SINE(R.string.pf_sine),
    PF_UNKNOWN(R.string.pf_unknown);

    override fun toString(): String = MikCubeApplication.applicationContext().getString(figureName)

    companion object {
        fun getByIndex(index:Byte) : Figures = when {
            index>=0 && index<PF_UNKNOWN.ordinal-> values()[index.toInt()]
            else -> PF_UNKNOWN
        }
    }
}
