package org.zamek.mikcube.model

import android.util.Log
import com.fasterxml.jackson.annotation.JsonIgnore
import org.zamek.mikcube.util.ColorArray
import org.zamek.mikcube.util.OFF_COLOR
import org.zamek.mikcube.util.OFF_COLOR_OPACITY
import java.util.ArrayList
import kotlin.math.abs

/**
 * \brief
 * \author Zoltan Zidarics (Zamek)
 * \date 2020
 */
class MCube {

    //If you want to use it in DEBUG mode temporally.
    // Must be beginnig with DEBUG_TEMPORARY because git can set it to false when commit
    @JsonIgnore
    private val DEBUG_TEMPORARY_TESTING = false

    private inline fun log(msg: String) {
        if (DEBUG_TEMPORARY_TESTING) Log.d("QQQ", msg)
    }

    companion object {
        const val MAX_ROWS = 8
        const val MAX_COLUMNS = 8
        const val PIXELS_PER_PLAIN = MAX_ROWS * MAX_COLUMNS
        const val MAX_FRAME_TIME_MS = 1000L
        const val MIN_FRAME_TIME_MS = 60L
        const val MAX_PLAINS = 8
        const val MAX_PIXELS = MAX_PLAINS*MAX_ROWS*MAX_COLUMNS
    }

    enum class PlainType {
        HORIZONTAL, VERTICAL
    }

    private var pixels : ArrayList<Pixel>

    var frameTime : Long = MIN_FRAME_TIME_MS
    private set(v) {
        if (v in MIN_FRAME_TIME_MS..MAX_FRAME_TIME_MS)
            field = v
    }

    init {
        pixels = ArrayList(MAX_PIXELS)
        for (i in 0 until MAX_PIXELS)
            pixels.add(Pixel())

        clear()
    }

    fun getColorArray(index: Int) : ColorArray = pixels[index].colorArray

    fun clear() {
        for (i in pixels.indices)
            pixels[i].setColor(OFF_COLOR, OFF_COLOR_OPACITY)
    }

    fun getPixel(index: Int): Pixel = pixels[index]

    private inline fun getPhysicalIndex(virtual: Int) : Int {
        val indexInPlain = virtual%PIXELS_PER_PLAIN
        return (indexInPlain%MAX_ROWS)*PIXELS_PER_PLAIN +
                (virtual/PIXELS_PER_PLAIN)*MAX_COLUMNS +
                (indexInPlain/MAX_COLUMNS)
    }

    fun setColor(index: Int, color: Int, opacity: Float) {
        if (index>=MAX_PIXELS)
            throw  Exception("setPixel index overflow: $index")
        log("------------------>>>AbstractCube.setColor, index:$index, physical:${getPhysicalIndex(index)}")
        getPixel(getPhysicalIndex(index)).setColor(color, opacity)
    }

    fun setColor(plain: Int, plainType: PlainType, row: Int, col: Int, color: Int, opacity: Float) {
        setColor(getIndex(plain, plainType, row, col), color, opacity)
    }

    fun setColor(plain: Int, plainType: PlainType, position: Int, color: Int, opacity: Float) {
        log("------------------>>>AbstractCube.setColor, plain:$plain, plainType:$plainType, position:$position, color:${color.toString(16)}, opacity:$opacity")
        setColor(getIndex(plain, plainType, position), color, opacity)
    }

    private fun getColor(index: Int) : Int {
        log("------------------>>>AbstractCube.getColor, index:$index, physical:${getPhysicalIndex(index)}")
        return getPixel(getPhysicalIndex(index)).color
    }

    fun getColor(plain: Int, plainType: PlainType, position: Int):Int {
        val result = getColor(getIndex(plain, plainType, position))
        log("------------------>>>AbstractCube.getColor, plain:$plain, plainType:$plainType, position:$position, color:${result.toString(16)}")
        return result
    }

    private fun getIndex(plain: Int, plainType: PlainType, row: Int, col: Int) = when (plainType) {
        PlainType.HORIZONTAL -> plain * PIXELS_PER_PLAIN + col * MAX_COLUMNS + row
        else -> row * PIXELS_PER_PLAIN + col * MAX_COLUMNS + plain
    }

    private fun getIndex(plain: Int, plainType: PlainType, position: Int) : Int {
        return getIndex(plain, plainType, position % MAX_COLUMNS, position/MAX_COLUMNS)
    }

    fun drawLine(plain: Int, plainType: PlainType, x0:Int, y0:Int, x1:Int, y1:Int, color: Int, opacity: Float) {

        if (abs(y1-y0) < abs(x1-x0)) {
            if (x0>x1)
                plotLineLow(plain,plainType,x1,y1,x0,y0,color,opacity)
            else
                plotLineLow(plain,plainType,x0,y0,x1,y1,color,opacity)
            return
        }
        if (y0>y1)
            plotLineHigh(plain,plainType,x1,y1,x0,y0,color,opacity)
        else
            plotLineHigh(plain,plainType,x0,y0,x1,y1,color,opacity)
    }

    fun drawCircle(plain:Int, plainType: PlainType, origoX:Int, origoY:Int, radius:Int, color: Int, opacity: Float) {
        var f:Int=1-radius
        var ddfX:Int=0
        var ddfY:Int=0
        var x:Int=0
        var y:Int=radius

        setColor(plain, plainType, origoX, origoY+radius, color, opacity)
        setColor(plain, plainType, origoX, origoY-radius, color, opacity)
        setColor(plain, plainType, origoX+radius, origoY, color, opacity)
        setColor(plain, plainType, origoX-radius, origoY, color, opacity)

        while(x<y) {
            if (f>=0) {
                --y
                ddfY += 2
                f += ddfY
            }
            ++x
            ddfX += 2
            f += ddfX + 1

            setColor(plain, plainType, origoX+x, origoY+y, color, opacity)
            setColor(plain, plainType, origoX-x, origoY+y, color, opacity)

            setColor(plain, plainType, origoX+x, origoY-y, color, opacity)
            setColor(plain, plainType, origoX-x, origoY-y, color, opacity)

            setColor(plain, plainType, origoX+y, origoY+x, color, opacity)
            setColor(plain, plainType, origoX-y, origoY+x, color, opacity)

            setColor(plain, plainType, origoX+y, origoY-x, color, opacity)
            setColor(plain, plainType, origoX-y, origoY-x, color, opacity)
        }
    }

    private fun plotLineLow(plain: Int, plainType: PlainType, x0:Int, y0:Int, x1:Int, y1:Int, color: Int, opacity: Float) {
        val dx:Int = x1-x0
        var dy:Int=y1-y0
        var yi:Int=1
        if (dy<0) {
            yi=-1
            dy=-dy
        }
        var d:Int=2*dy-dx
        var y:Int=y0
        for(x in x0..x1) {
            setColor(plain, plainType, x, y, color,opacity)
            if (d>0) {
                y+=yi
                d -= 2 * dx
            }
            d += 2 * dy
        }
    }

    private fun plotLineHigh(plain: Int, plainType: PlainType, x0:Int, y0:Int, x1:Int, y1:Int, color: Int, opacity: Float) {
        var dx:Int=x1-x0
        val dy:Int=y1-y0
        var xi:Int=1
        if (dx<0) {
            xi=-1
            dx=-dx
        }
        var d:Int=2*dx-dy
        var x:Int=x0
        for (y in y0..y1) {
            setColor(plain, plainType, x, y, color, opacity)
            if (d>0) {
                x+=xi
                d -= 2 * dy
            }
            d += 2 * dx
        }
    }

}