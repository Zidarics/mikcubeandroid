package org.zamek.mikcube.opengl

import android.opengl.GLES20
import android.opengl.GLSurfaceView
import android.opengl.Matrix
import android.util.Log
import org.zamek.mikcube.model.MCube
import org.zamek.mikcube.util.ColorArray
import org.zamek.mikcube.util.createOffColorArray
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.*
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

/**
 * \brief
 * \author Alexrnov
 * \date 2020
 */
class SceneRenderer(private val versionGL: Int=0) : GLSurfaceView.Renderer {

    //If you want to use it in DEBUG mode temporally.
    // Must be beginnig with DEBUG_TEMPORARY because git can set it to false when commit
    private val DEBUG_TEMPORARY_TESTING = false

    private inline fun log(msg: String) {
        if (DEBUG_TEMPORARY_TESTING) Log.d("QQQ", msg)
    }

    // coefficients for camera rotation
    private var kx = 0f
    private var ky = 0f

    /* coordinates of camera */
    private var xCamera = 0.0f
    private var yCamera = 0.0f
    private var zCamera = 2.6f

    /* coordinates of camera for thread-safe distance calculations */
    private var xCamera2 = 0.0f
    private var yCamera2 = 0.0f
    private var zCamera2 = 2.6f

    private val cubes = arrayOfNulls<Cube>(MCube.MAX_PIXELS)

    private val viewMatrix = FloatArray(16)
    private val projectionMatrix = FloatArray(16)

    private var initCubes = false // this flag check if all objects was init

    private val defaultColor: ColorArray = createOffColorArray()

    private val VBO = IntArray(1)

    @Volatile
    private var changeView = false // this flag check if camera was moved

    private val transparentObjects: MutableList<Cube> = ArrayList()

    // sort cubes by distance to camera for correct alpha blending
    private val comparatorByZ = Comparator { objectA: Cube, objectB: Cube ->
        val cameraDistance1 = Math.sqrt(
            Math.pow(
                (xCamera2 - objectA.getX()).toDouble(), 2.0
            )
                    + Math.pow((yCamera2 - objectA.getY()).toDouble(), 2.0)
                    + Math.pow((zCamera2 - objectA.getZ()).toDouble(), 2.0)
        )
        val cameraDistance2 = Math.sqrt(
            Math.pow((xCamera2 - objectB.getX()).toDouble(), 2.0)
                    + Math.pow((yCamera2 - objectB.getY()).toDouble(), 2.0)
                    + Math.pow((zCamera2 - objectB.getZ()).toDouble(), 2.0)
        )
        cameraDistance2.compareTo(cameraDistance1)
    }

    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
        val size = 0.024f // size of cube
        val vertices = floatArrayOf(
            -size, size, size, -size, -size, size, size, -size, size, size, -size, size,
            size, size, size, -size, size, size, -size, size, -size, -size, -size, -size,
            size, -size, -size, size, -size, -size, size, size, -size, -size, size, -size,
            -size, size, -size, -size, -size, -size, -size, -size, size, -size, -size, size,
            -size, size, size, -size, size, -size, size, size, -size, size, -size, -size,
            size, -size, size, size, -size, size, size, size, size, size, size, -size,
            -size, size, -size, -size, size, size, size, size, size, size, size, size,
            size, size, -size, -size, size, -size, -size, -size, -size, -size, -size, size,
            size, -size, size, size, -size, size, size, -size, -size, -size, -size, -size
        )
        val bufferVertices = ByteBuffer.allocateDirect(vertices.size * 4)
            .order(ByteOrder.nativeOrder()).asFloatBuffer()
        bufferVertices.put(vertices).position(0)
        val vShader: String
        val fShader: String
        if (versionGL == 2) { // version is OpenGL ES 2.0
            vShader = """
            #version 100                                            
            uniform mat4 mvp_matrix;                       
            attribute vec4 a_position;                        
            void main()                                               
            {                                                              
            gl_Position = mvp_matrix * a_position;  
            }                                                              
            
            """.trimIndent()
            fShader = """
            #version 100                                           
            precision lowp float;                                 
            uniform vec4 v_color;                              
            void main()                                              
            {                                                              
            gl_FragColor = v_color;                         
            }                                                              
            
            """.trimIndent()
        } else { // version OpenGL ES 3.0 or higher
            vShader = """
            #version 300 es                                      
            uniform mat4 mvp_matrix;                      
            in vec4 a_position;                                   
            void main()                                               
            {                                                              
            gl_Position = mvp_matrix * a_position;  
            }                                                              
            
            """.trimIndent()
            fShader = """
            #version 300 es                                      
            precision lowp float;                                
            uniform vec4 v_color;                              
            out vec4 fragColor;                                  
            void main()                                               
            {                                                              
            fragColor = v_color;                               
            }                                                               
            
            """.trimIndent()
        }
        val linkedProgram = LinkedProgram(vShader, fShader)
        val programObject = linkedProgram.get()
        Matrix.setLookAtM(
            viewMatrix, 0, xCamera, yCamera, zCamera,
            0.0f, 0.0f, 0f, 0f, 1.0f, 0.0f
        )
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f)
        // implementation prioritizes performance
        GLES20.glHint(GLES20.GL_GENERATE_MIPMAP_HINT, GLES20.GL_FASTEST)
        VBO[0] = 0
        GLES20.glGenBuffers(1, VBO, 0)
        bufferVertices.position(0)
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, VBO[0])
        // 12 is the float_size (4) * component of vertex (3), 36 is the number of vertex
        GLES20.glBufferData(
            GLES20.GL_ARRAY_BUFFER, 12 * 36,
            bufferVertices, GLES20.GL_STATIC_DRAW
        )
        var i = 0 // id of cube
        for (kz in -4..3) {
            val z = -kz * 0.10f - 0.04f // subtract the 0.04 value to center the scene
            for (ky in -4..3) {
                val y = ky * 0.10f - 0.04f
                for (kx in 4 downTo -4 + 1) { // start position of lowest right angle
                    val x = kx * 0.10f - 0.04f
                    val cube = Cube(programObject)
                    cubes[i] = cube
                    transparentObjects.add(cube)
                    cubes[i]!!.setPosition(x, y, z)
                    cubes[i]!!.setColor(defaultColor)
                    i++
                }
            }
        }
        initCubes = true // init finish
        // set default camera angle
        setMotion(900.0f, -350.0f)
    }

    // screen orientation change handler, also called when returning to the app
    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        GLES20.glViewport(0, 0, width, height) // set screen size
        val aspect = width.toFloat() / height.toFloat()
        val k = 1f / 30 // coefficient is selected empirically
        if (width < height) { // portrait orientation
            Matrix.frustumM(
                projectionMatrix, 0, -1f * k, 1f * k,
                1 / -aspect * k, 1 / aspect * k, 0.1f, 40f
            )
        } else { // landscape orientation
            Matrix.frustumM(
                projectionMatrix, 0, -aspect * k,
                aspect * k, -1f * k, 1f * k, 0.1f, 40f
            )
        }

        /* set default view for scene */for (cube in transparentObjects) cube.defineView(
            viewMatrix,
            projectionMatrix
        )

        // sort by length to camera for correct transparency
        Collections.sort(transparentObjects, comparatorByZ)
    }

    // called when the frame is redrawn
    override fun onDrawFrame(gl: GL10?) {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT or GLES20.GL_DEPTH_BUFFER_BIT)
        GLES20.glEnable(GLES20.GL_DEPTH_TEST) // enable depth test
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, VBO[0])

        /* when used blending */GLES20.glEnable(GLES20.GL_BLEND)
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA)
        if (changeView) { // if the camera was moved then perform matrix calculation
            // apply immutable matrix to avoid flicker artifact
            val immutableViewMatrix = Arrays.copyOf(viewMatrix, 16)
            for (cube in transparentObjects) cube.defineView(immutableViewMatrix, projectionMatrix)
            changeView = false

            // copy coordinates of camera that no happen thread error
            xCamera2 = xCamera
            yCamera2 = yCamera
            zCamera2 = zCamera
            // sort by length to camera for correct transparency
            Collections.sort(transparentObjects, comparatorByZ)
        }
        for (i in transparentObjects.indices) {
            transparentObjects[i].draw()
        }
        GLES20.glDisable(GLES20.GL_BLEND)

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0)
    }

    /** Set camera in default place  */
    @Synchronized
    fun defaultView() {
        xCamera = 0f
        yCamera = 0f
        zCamera = 2.6f
        kx = 0f
        ky = 0f
        Matrix.setLookAtM(
            viewMatrix, 0, xCamera, yCamera, zCamera,
            0f, 0f, 0f, 0f, 1.0f, 0.0f
        )
        // set default camera angle
        setMotion(900.0f, -350.0f)
    }

    /**
     * Move camera based on offset by X and Y
     * @param xDistance - X-axis offset
     * @param yDistance - Y-axis offset
     */
    @Synchronized
    fun setMotion(xDistance: Float, yDistance: Float) {
        //Log.i("P", "xDistance = " + xDistance + ", yDistance =" + yDistance);
        kx += xDistance * 0.001f
        // limit rotation to z
        if ((ky >= -0.5 || yDistance >= 0.0) && (ky <= 0.5 || yDistance < 0.0)) {
            ky += yDistance * 0.001f
        }
        val radius = 2.6f // radius of rotation of the camera around the object
        // define spherical coordinates for camera
        xCamera = (radius * Math.cos(ky.toDouble()) * Math.sin(kx.toDouble())).toFloat()
        yCamera = (radius * Math.sin(ky.toDouble())).toFloat()
        zCamera = (radius * Math.cos(ky.toDouble()) * Math.cos(kx.toDouble())).toFloat()

        // set position for camera
        Matrix.setLookAtM(
            viewMatrix, 0, xCamera, -yCamera, zCamera,
            0f, 0.0f, 0f, 0f, 1.0f, 0.0f
        )
        changeView = true // in next frame perform calculation matrix
    }

    /**
     * The method checks if all cubes are initialized.
     * @return <value>true</value> - if all cubes is init,
     * <value>else</value> - in another case
     */
    @Synchronized
    fun isLoad(): Boolean {
        return initCubes
    }

    /**
     * Set current color for cube.
     * @param i - id of cube
     * @param color - current color of cube
     */
    @Synchronized
    fun setColor(i: Int, color: ColorArray) {
        cubes[i]!!.setColor(color)
    }

}
