package org.zamek.mikcube.opengl

import android.opengl.GLES20
import android.opengl.Matrix
import org.zamek.mikcube.util.ColorArray
import org.zamek.mikcube.util.createColorArray

/**
 * \brief
 * \author Alexrnov
 * \date 2020
 */
/**
 * A class for creating a separate cube that can change color. No indices or
 * normals are used for the cube. It also does not use shader lighting
 * computation and buffer objects. This is due to the goal of improving
 * performance, since there can be many cubes. Additional data and objects
 * can increase memory consumption and object initialization time.
 */

class Cube(val programObject: Int) {

    private var mvpMatrixLink  = 0  // link of uniform for mvpMatrix
    private var colorLink = 0  // link of uniform for color
    private var positionLink  = 0  // link of vertex attribute

    private val mvpMatrix = FloatArray(16)
    private val modelMatrix = FloatArray(16)

    private var color : ColorArray = createColorArray() // color of cube

    private var x = 0.0f
    private var y = 0.0f
    private var z = 0.0f

    init {
        mvpMatrixLink = GLES20.glGetUniformLocation(this.programObject, "mvp_matrix")
        colorLink = GLES20.glGetUniformLocation(this.programObject, "v_color")
        positionLink = GLES20.glGetAttribLocation(programObject, "a_position")
    }

    /**
     * Define view of cube.
     * @param viewMatrix - matrix of view (change when changes when the cube rotates)
     * @param projectionMatrix - projection matrix (change when the screen rotates)
     */
    fun defineView(viewMatrix: FloatArray?, projectionMatrix: FloatArray?) {
        Matrix.multiplyMM(mvpMatrix, 0, viewMatrix, 0, modelMatrix, 0)
        Matrix.multiplyMM(mvpMatrix, 0, projectionMatrix, 0, mvpMatrix, 0)
    }

    /**
     * Draw object. Methods such as: setPosition(), setColor(), setView()
     * must be called first.
     */
    fun draw() {
        GLES20.glUseProgram(this.programObject)
        GLES20.glUniformMatrix4fv(mvpMatrixLink, 1, false, mvpMatrix, 0)
        GLES20.glEnableVertexAttribArray(positionLink) // allow cube vertices attribute

        // in case without VBO
        GLES20.glVertexAttribPointer(
            positionLink, 3, GLES20.GL_FLOAT,
            false, 12, 0
        )
        GLES20.glUniform4fv(colorLink, 1, color[0], 0) // pass color of face to shader
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 36)

        GLES20.glDisableVertexAttribArray(positionLink) // disable cube vertices attribute
    }

    /**
     * Set color for cube.
     * @param color - current color
     */
    fun setColor(color: ColorArray) {
        this.color = color
    }

    /**
     * Define start position for cube.
     * @param x - coordinate x
     * @param y - coordinate y
     * @param z - coordinate z
     */
    fun setPosition(x: Float, y: Float, z: Float) {
        this.x = x
        this.y = z
        this.z = z
        Matrix.setIdentityM(modelMatrix, 0) // reset matrix to one
        Matrix.translateM(modelMatrix, 0, x, y, z) // move cube
    }

    fun getX(): Float = x

    fun getY(): Float = y

    fun getZ(): Float = z

}
