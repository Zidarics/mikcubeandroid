package org.zamek.mikcube.opengl

import android.annotation.SuppressLint
import android.content.Context
import android.opengl.GLSurfaceView
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.GestureDetector.OnDoubleTapListener
import android.view.MotionEvent
import androidx.core.view.GestureDetectorCompat

/**
 * \brief
 * \author Alexrnov
 * \date 2020
 */
class SurfaceView : GLSurfaceView, GestureDetector.OnGestureListener, OnDoubleTapListener {

    var renderer: SceneRenderer? = null
    private var detector: GestureDetectorCompat? = null

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attributes: AttributeSet?) : super(context, attributes)

    fun init(versionGLES: Int, context: Context?) {
        preserveEGLContextOnPause = true // save context OpenGL
        // Tell the OGLView container that we want to create an OpenGL ES 2.0/3.0
        // compatible context and install an OpenGL ES 2.0/3.0 compatible render
        setEGLContextClientVersion(versionGLES)
        renderer = SceneRenderer(versionGLES)
        setRenderer(renderer)
        renderMode = RENDERMODE_WHEN_DIRTY
        detector = GestureDetectorCompat(context, this)
        detector!!.setOnDoubleTapListener(this) // set gesture detector as double tap listener
    }

    fun restorePosition() {
        renderer!!.defaultView()
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(e: MotionEvent?): Boolean {
        return if (detector!!.onTouchEvent(e)) {
            true
        } else super.onTouchEvent(e)
    }

    override fun onScroll(
        event1: MotionEvent?, event2: MotionEvent?,
        distanceX: Float, distanceY: Float
    ): Boolean {
        renderer!!.setMotion(distanceX, distanceY) // camera rotation
        return true
    }

    override fun onDoubleTapEvent(motionEvent: MotionEvent?): Boolean {
        renderer!!.defaultView() // return the camera to default view
        return true
    }

    override fun onDown(event: MotionEvent?) = true

    override fun onSingleTapConfirmed(motionEvent: MotionEvent?) = true

    override fun onDoubleTap(motionEvent: MotionEvent?) = true

    override fun onShowPress(motionEvent: MotionEvent?) {
        //NC
    }

    override fun onSingleTapUp(motionEvent: MotionEvent?) = true

    override fun onLongPress(motionEvent: MotionEvent?) {
        //NC
    }

    override fun onFling(
        motionEvent1: MotionEvent?, motionEvent2: MotionEvent?,
        v1: Float, v2: Float
    ) = true

    fun getSceneRenderer(): SceneRenderer? = renderer

}
